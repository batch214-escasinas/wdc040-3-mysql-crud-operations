INSERT INTO `users`( `email`, `password`, `datetime_created`) VALUES ('johnsmith@gmail.com','passwordA',CURRENT_TIMESTAMP );
INSERT INTO `users`( `email`, `password`, `datetime_created`) VALUES ('juandelacruz@gmail.com','passwordB',CURRENT_TIMESTAMP );
INSERT INTO `users`( `email`, `password`, `datetime_created`) VALUES ('janessmith@gmail.com','passwordC',CURRENT_TIMESTAMP );
INSERT INTO `users`( `email`, `password`, `datetime_created`) VALUES ('mariadelacruz@gmail.com','passwordD',CURRENT_TIMESTAMP );
INSERT INTO `users`( `email`, `password`, `datetime_created`) VALUES ('johndoe@gmail.com','passwordE',CURRENT_TIMESTAMP );


INSERT INTO `posts`( `user_id`, `title`, `content`, `datetime_posted`) VALUES (1,'First Code','Hello World!',CURRENT_TIMESTAMP);
INSERT INTO `posts`( `user_id`, `title`, `content`, `datetime_posted`) VALUES (1,'Second Code','Hello Earth!',CURRENT_TIMESTAMP);
INSERT INTO `posts`( `user_id`, `title`, `content`, `datetime_posted`) VALUES (2,'Third Code','Welcome to Mars!',CURRENT_TIMESTAMP);
INSERT INTO `posts`( `user_id`, `title`, `content`, `datetime_posted`) VALUES (4,'Fouth Code','Bye bye solar system!',CURRENT_TIMESTAMP);

SELECT * FROM `posts` WHERE `user_id`=1

SELECT `email`,`datetime_created` FROM `users`

UPDATE `posts` SET `content`='Hello to the people of the Earth!' WHERE `id`=2

DELETE FROM `users` WHERE `email`='johndoe@gmail.com'